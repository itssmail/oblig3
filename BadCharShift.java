import java.util.*;

public class BadCharShift {
    char[] needle, haystack;
    public static void main(String[] args) {
	String needle_file = args[0];
	String haystack_file = args[1];
	Scanner needlescanner = new Scanner (BadCharShift.class.getResourceAsStream(needle_file), "UTF-8");
	Scanner haystackscanner = new Scanner (BadCharShift.class.getResourceAsStream(haystack_file), "UTF-8");
	String needle_string = needlescanner.next();
	String haystack_string = haystackscanner.next();
	
	System.out.println("Needle is : " + needle_string + " Haystack is : " + haystack_string);
	char[] needle = needle_string.toCharArray();
	char[] haystack = haystack_string.toCharArray();
	

	//char[] haystack = cogwrgaccag"];
	//System.out.println(haystack[0]);
	BadCharShift bcs = new BadCharShift(needle,haystack);
	int[] found = bcs.boyer_moore_horspool(needle, haystack);
	for (int i = 0; i < found.length - 1; i++)
	    //if (i != 0 && found[i] != 0) {
	    System.out.println("Match found at haystack[" + found[i] + "]");
	//}
    } //END static void main
    
    //simple constructor
    BadCharShift(char[] needle, char[] haystack) {
	this.needle = needle;
	this.haystack = haystack;
    }

    int CHAR_MAX = 256;
    public int[] boyer_moore_horspool (char[] needle, char[] haystack) {
	int[] findings = new int[haystack.length];
	System.out.println("Length of findings array is :" + findings.length);	
	int[] matches = new int[haystack.length];
	int j = 0;
	if ( needle.length > haystack.length ){ return null; }
	int[] bad_shift = new int[CHAR_MAX]; // 256
	for(int i = 0; i < CHAR_MAX; i++){
	    bad_shift[i] = needle.length;
	}
	int offset = 0, scan = 0;
	int last = needle.length - 1;
	int maxoffset = haystack.length - needle.length;
	for(int i = 0; i < last; i++){
	    bad_shift[needle[i]] = last - i;
	}
	while(offset <= maxoffset){
	    for(scan = last; needle[scan] == haystack[scan+offset]; scan--){
		//for (int j = 0; j < haystack.length ; j++)
		if(scan == 0){ // match found!
		    // add the index to an array and keep searching
		    System.out.println("Match at : " + offset );
		    matches[j++] = offset;
		    offset += needle.length;
		    //scan = last;
		    scan = last;
		    //return offset;
		}
		if (scan+offset >= haystack.length) return matches;
							      
	    }
	    //System.out.println("Haytack of offset + last : " + haystack[offset + last]);
	    //System.out.println("Haytack of offset + last : " + bad_shift[haystack[offset + last]]);
	    if (scan+offset <= haystack.length) {
		offset += bad_shift[haystack[offset + last]];
	    }
	}
	return matches;
	//return null;
    }

} //END class